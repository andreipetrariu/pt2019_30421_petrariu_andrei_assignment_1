package pack2;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pack1.Monomial;

public class Polynomial {
		private List<Monomial> polynomial;
		public Polynomial() {
			polynomial=new ArrayList<Monomial>();
		}
		public void setPolynom(int[] p,int[] c) {
			int l=p.length;
			int i=0;
			Monomial m=null;
			while(i<l) {
				if(c[i]!=0) {
				m=new Monomial(p[i],c[i]);
				polynomial.add(m);
				}
				i++;
			}
		}
		public List<Monomial> getPolynom()
		{
			return polynomial;
		}
		public void addMonom(Monomial m) {
			if(m.getC()!=0)
				polynomial.add(m);
		}
		public Polynomial addAndSubtract(Polynomial pol,int operation) {  //operation=0 when adding, 1 when subtracting
			Iterator<Monomial> it=polynomial.iterator();
			Polynomial result=new Polynomial();
			int l=pol.getPolynom().size();
			int[] added=new int[l]; //an array with the same length as the second polynomial used to find the terms which haven't been added/subtracted
		
			while(it.hasNext()) { //each element of the first polynomial is saved into term1.If it is equal to any term in the second polynomial(pol),then their coefficients are added
				//if no equal power terms are found,term1 remains unchanged.Finally term1 is added to the result
				Monomial temp=it.next();
				Monomial term1=new Monomial(temp.getP(),temp.getC());
				Monomial term2=null;//can't use for each because index i is used in added array
				for(int i=0;i<l;i++) {  //going through pol to check if any element has the same power as term1
					term2=pol.getPolynom().get(i);
					if(term1.compareTo(term2)==0)
						{if(operation==0)term1.setCoefficient(term1.getC()+term2.getC());//coefficient are added
						else term1.setCoefficient(term1.getC()-term2.getC());//or subtracted
						added[i]=1;}
				}
				result.addMonom(term1);
			}
			for(int i=0;i<l;i++) { //adding to the result the remaining terms in the second polynomial
				Monomial m=pol.getPolynom().get(i);
				if(added[i]!=1) {
					Monomial temp=new Monomial(m.getP(),m.getC());
					if(operation==1){ temp.setCoefficient(-m.getC());m.setPower(m.getP());}
					result.addMonom(temp);
					}
			}
			return result;
}
		public String getStringPolynom() {//this method is used in the model to output the polynomial as a string
			String sPol="";
			boolean first=true;
			DecimalFormat f=new DecimalFormat("0.#");
			for(Monomial m:polynomial){
				if(first) {
					sPol=sPol+f.format(m.getC()) +"x^"+ f.format(m.getP())+" ";
					first=false;
				}
				else sPol=sPol+(m.getC()<0 ? "" : "+")+f.format(m.getC())+"x^"+f.format(m.getP())+" ";
			}
			return sPol.trim();//removing the space at the end
		}
		public Polynomial multiply(Polynomial pol) {
			Polynomial result=new Polynomial();
			Polynomial helper; //'helper' keeps the polynomial obtained by multiplying one term of the first polynomial with pol, the second one.After obtaining 'helper', it is added to the result polynomial and initialised again
			Iterator<Monomial> it=polynomial.iterator();
			while(it.hasNext()) {
				boolean first=true;//'first' is a condition which is true if the while is in the first iteration.
				Monomial term1=it.next();
				Monomial product;
				helper=new Polynomial();
				for(Monomial term2 : pol.getPolynom()) {
					product=new Monomial(term1.getP()+term2.getP(),term1.getC()*term2.getC());
					helper.addMonom(product);
				}
					result=result.addAndSubtract(helper, 0);
			}
			return result;
		}
		public Polynomial[] divide(Polynomial pol) {
			if(pol.getPolynom()==null)
				return null;
			Polynomial[] result=new Polynomial[2];//quotient and remainder polynomials
			Polynomial t;//a one term polynomial which keeps the result of the division between the greatest degree terms in every for iteration. The reason it isn't a monomial is because it must be added and multiplied with other polynomials
			Monomial term1,term2;//dividend and divisor
			result[0]=new Polynomial();//quotient
			result[1]=new Polynomial();
			for(Monomial m:this.getPolynom())
				result[1].addMonom(new Monomial(m.getP(),m.getC()));//copying the dividend into the remainder
			boolean first=true;
			while(result[1].getHighestDegree().getP()>=pol.getPolynom().get(0).getP()){//term1 is the term of the remainder
				term1=result[1].getHighestDegree();
				term2=pol.getPolynom().get(0);
				if(first) {
					result[0].addMonom(new Monomial(term1.getP()-term2.getP(),term1.getC()/term2.getC()));
					result[1]=result[1].addAndSubtract(pol.multiply(result[0]), 1);
					first=false;
					}
				else{t=new Polynomial();
				t.addMonom(new Monomial(term1.getP()-term2.getP(),term1.getC()/term2.getC()));
				result[0]=result[0].addAndSubtract(t,0);
				result[1]=result[1].addAndSubtract(t.multiply(pol), 1);
				
			}}
			return result;
		}
		public Monomial getHighestDegree()
		{
			Monomial n=new Monomial();
			for(Monomial i:polynomial) {
				if(i.getP()>=n.getP() && !(i.getC()==0 && i.getP()==0)) {//second condition means it skips 0x^0 terms which may be hidden in the polynomials. If for example there's a -5x^0 term, we want to take it, not the 0x^0 term
					n.setPower(i.getP());
					n.setCoefficient(i.getC());
				}
			}
			return n;
		}
		public Polynomial difAndIntegrate(int operation) {
			Polynomial result=new Polynomial();
			for(Monomial term : polynomial) {
				if(operation==0)//operation=0 for differentiation, otherwise the function integrates
					result.addMonom(new Monomial(term.getP()-1,term.getC()*term.getP()));
				else result.addMonom(new Monomial(term.getP()+1,term.getC()/(term.getP()+1)));
			}
			return result;
		}
}
