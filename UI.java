package ui;
import pack2.Polynomial;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Random;
import javax.swing.*;

public class UI {
	public int[][] convert(String in)//used in the test case
	{
		return Model.convertHelper(in);
	}
	static class Model {
		private Polynomial resultP;
		private Polynomial remainderP;//used only in the division to keep the remainder
		public Model() {
			resultP=new Polynomial();
			remainderP=new Polynomial();
		}
		public String getResult() {
			return resultP.getStringPolynom();
		}
		public String getRemainder()
		{
			return remainderP.getStringPolynom();
		}
		public void setResult(Polynomial resultP) {
			this.resultP=resultP;
		}
		public void setRemainder(Polynomial remainderP)
		{
			this.remainderP=remainderP;
		}
		public static int[][] convertHelper(String in) {//helper method for convertInput
			int[][] result;
			int[] p,c;p=new int[10];c=new int[10];
			int i=0,j=0,termLength, xIndex;
			Pattern pattern = Pattern.compile("([+-]?(?:(?:\\d+x\\^[+-]?\\d+)|(?:\\d+x)|(?:\\d+)|(?:x\\^[+-]?\\d+)|(?:x)))");
			Matcher matcher = pattern.matcher(in);
			String term;
			while (matcher.find()) {
				xIndex=-1;
				term=matcher.group(1);
				termLength=term.length();
				while(xIndex==-1 && j<termLength) {
					if(term.charAt(j)=='x')
						xIndex=j;
					j++;
				}
				if(xIndex==-1) {
				p[i]=0;
				c[i]=Integer.parseInt(term.substring(0,termLength));
				} else {
					if(term.charAt(termLength-1)=='x' || term.charAt(termLength-1)=='x')
						p[i]=1;
					else p[i]=Integer.parseInt(term.substring(xIndex+2, termLength));
					if((term.charAt(0)=='x') || term.substring(0,xIndex+1).charAt(1)=='x' && (term.substring(0,xIndex).charAt(0)=='+' || term.substring(0,xIndex).charAt(0)=='-'))
						c[i]=1;
					else c[i]=Integer.parseInt(term.substring(0,xIndex));
				}i++; j=0;
			}
			result=new int[2][i];
			for(j=0;j<i;j++) {
			result[0][j]=p[j];
			result[1][j]=c[j];
			}
			return result;
		}
		public static int[][] convertInput(String in1,String in2) {
			int[][] thePolynomials=new int[4][];
			int[][] result1=Model.convertHelper(in1);
			int[][] result2=Model.convertHelper(in2);
			thePolynomials[0]=result1[0].clone();//powers array 1
			thePolynomials[1]=result1[1].clone();//coefficients array 1
			thePolynomials[2]=result2[0].clone();//powers array 2
			thePolynomials[3]=result2[1].clone();//coefficients array 2
			return thePolynomials;
		}
		public void calculate(String in1,String in2,String operation) {
			int[][] thePolynomials=convertInput(in1,in2);//thePolynomials:2D array which keeps the input coefficients and powers.First 2 lines are the coefficient and power array pair of the first input,and the next 2 lines the array pair of the second input
			Polynomial pol1,pol2; pol1=new Polynomial(); pol2=new Polynomial();
			pol1.setPolynom(thePolynomials[0], thePolynomials[1]);
			pol2.setPolynom(thePolynomials[2], thePolynomials[3]);
			switch(operation) {
			case "Add":this.setResult(pol1.addAndSubtract(pol2, 0));break;
			case "Subtract":this.setResult(pol1.addAndSubtract(pol2, 1));break;
			case "Multiply":this.setResult(pol1.multiply(pol2));break;
			case "Divide":Polynomial[] result=pol1.divide(pol2);this.setResult(result[0]);this.setRemainder(result[1]);break;
			case "Differentiate":this.setResult(pol1.difAndIntegrate(0));break;
			case "Integrate":this.setResult(pol1.difAndIntegrate(1));break;
			default:break;
			}
		}
	}
	static class View extends JFrame {
		private String[] ops= {"Add","Subtract","Multiply","Divide","Differentiate","Integrate"};
		private JLabel title=new JLabel("Please enter the variables using the format ax^b, a, b "+"\u2208 "+"\u2124");
		private JButton solveBtn=new JButton("=");
		private JTextField input1=new JTextField(18);
		private JTextField input2=new JTextField(18);
		private JTextField outputTF=new JTextField();
		private JComboBox<String> opList=new JComboBox<String>(ops);
		private Random randomColor;
		private Color bgColor;
		private float r,g,b;
		public View() {
			JPanel inputPanel=new JPanel();
			JPanel content=new JPanel();
			inputPanel.setLayout(new FlowLayout());
			content.setLayout(new BorderLayout());
			inputPanel.add(input1);
			inputPanel.add(opList);
			inputPanel.add(input2);
			inputPanel.add(solveBtn);
			content.add(title,BorderLayout.NORTH);
			title.setHorizontalAlignment(JLabel.CENTER);
			content.add(inputPanel,BorderLayout.CENTER);
			content.add(outputTF,BorderLayout.SOUTH);
			randomColor=new Random();
			r=randomColor.nextFloat();
			g=randomColor.nextFloat();
			b=randomColor.nextFloat();
			bgColor=new Color(r,g,b);
			double foo = (299 * bgColor.getRed() + 587 * bgColor.getGreen() + 114 * bgColor.getBlue()) / 1000;
			title.setForeground( foo >= 128 ? Color.BLACK : Color.WHITE);
			content.setBackground(bgColor);
			inputPanel.setBackground(bgColor);
			this.setTitle("Polynomial Calculator");
			this.setContentPane(content);
			this.setPreferredSize(new Dimension(570,200));
			this.pack();
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		public String getTF(int select)
		{
			if(select==0)
				return input1.getText();
			else return input2.getText();
		}
		public String getListItem()
		{
			return String.valueOf(opList.getSelectedItem());
		}
		public void showOutput(String output)
		{
			outputTF.setText(output);
		}
		public void addBtnListener(MouseListener ml)
		{
			solveBtn.addMouseListener(ml);
		}
	}
	static class Controller {
		private Model model;
		private View view;
		public Controller(Model aModel,View aView)
		{
			model=aModel;
			view=aView;
			view.addBtnListener(new BtnListener());
		}
		class BtnListener implements MouseListener {
				@Override
				public void mouseClicked(MouseEvent ev) {
					String in1,in2,opListItem,output;
					in1=view.getTF(0);
					in2=view.getTF(1);
					opListItem=view.getListItem();
					try {
						model.calculate(in1,in2,opListItem);
					}
					catch(Exception e)
					{
						JOptionPane.showMessageDialog(view,"Input conversion error/division by 0!");
					}
					output=model.getResult();
					if(opListItem=="Divide")
						output=output+",remainder="+model.getRemainder();
					view.showOutput(output);
				}
				@Override
				public void mouseEntered(MouseEvent e) {}

				@Override
				public void mouseExited(MouseEvent e) {}

				@Override
				public void mousePressed(MouseEvent e) {}

				@Override
				public void mouseReleased(MouseEvent e) {}
		}
	}
	public static void main(String[] args)
	{
		//demo
		Model m=new Model();
		View v=new View();
		Controller c=new Controller(m,v);
		v.setVisible(true);
	}
}
