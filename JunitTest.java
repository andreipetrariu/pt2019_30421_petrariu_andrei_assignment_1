package test;
import pack2.Polynomial;
import ui.UI;
public class JunitTest {
	public Polynomial add(Polynomial pol1,Polynomial pol2)
	{
		return pol1.addAndSubtract(pol2, 0);
	}
	public Polynomial subtract(Polynomial pol1,Polynomial pol2)
	{
		return pol1.addAndSubtract(pol2, 1);
	}
	public Polynomial multiply(Polynomial pol1,Polynomial pol2)
	{
		return pol1.multiply(pol2);
	}
	public Polynomial[] divide(Polynomial pol1,Polynomial pol2)
	{
		return pol1.divide(pol2);
	}
	public Polynomial differentiate(Polynomial pol)
	{
		return pol.difAndIntegrate(0);
	}
	public Polynomial integrate(Polynomial pol)
	{
		return pol.difAndIntegrate(1);
	}
}
