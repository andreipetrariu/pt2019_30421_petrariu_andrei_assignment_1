package pack1;

public class Monomial implements Comparable<Monomial>{
	private float p,  c;
	public Monomial()
	{
		p=0;c=0;
	}
	public Monomial(float power,float coefficient)
	{
		p=power;
		c=coefficient;
	}
	public void setPower(float power)
	{
		p=power;
	}
	public void setCoefficient(float coefficient)
	{
		c=coefficient;
	}
	public float getP()
	{
		return p;
	}
	public float getC()
	{
		return c;
	}
	public int compareTo(Monomial m) //compares two monomials by their powers
	{
		if(p<m.getP())
			return -1;
		else if(p>m.getP())
				return 1;
		return 0;
	}
}
