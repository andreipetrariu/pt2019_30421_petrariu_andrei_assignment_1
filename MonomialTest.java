package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import pack1.Monomial;
class MonomialTest {

	@Test
	void test() {
		Monomial m1=new Monomial(1,2);
		Monomial m2=new Monomial(4,1);
		assertEquals("1.0 2.0 4.0 1.0",m1.getP()+" "+m1.getC()+" "+m2.getP()+" "+m2.getC());
		m1.setCoefficient(10);
		m2.setPower(6);
		assertEquals("1.0 10.0 6.0 1.0",m1.getP()+" "+m1.getC()+" "+m2.getP()+" "+m2.getC());
		assertEquals(-1,m1.compareTo(m2));
		assertEquals(1,m2.compareTo(m1));
	}
}
