package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import pack2.Polynomial;

class PolynomialTest {

	@Test
	void test() {
		JunitTest test=new JunitTest();
		Polynomial pol1=new Polynomial();
		Polynomial pol2=new Polynomial();
		assert(pol1.getPolynom().isEmpty()==true);
		assert(pol2.getPolynom().isEmpty()==true);
		int[] p1= {3,2,1};
		int[] c1= {2,2,0};
		int[] p2= {5,3,2,0};
		int[] c2= {1,1,0,3};
		pol1.setPolynom(p1,c1);
		pol2.setPolynom(p2,c2);
		assertEquals("2x^3 +2x^2",pol1.getStringPolynom());
		assertEquals("1x^5 +1x^3 +3x^0",pol2.getStringPolynom());
		Polynomial output=test.add(pol1, pol2);
		assertEquals("3x^3 +2x^2 +1x^5 +3x^0",output.getStringPolynom());
		output=test.subtract(pol1, pol2);
		assertEquals("1x^3 +2x^2 -1x^5 -3x^0",output.getStringPolynom());
		output=test.multiply(pol1, pol2);
		assertEquals("2x^8 +2x^6 +6x^3 +2x^7 +2x^5 +6x^2",output.getStringPolynom());
		Polynomial[] outputs=test.divide(pol2, pol1);//pol2 divided by pol1
		assertEquals("0.5x^2 -0.5x^1 +1x^0",outputs[0].getStringPolynom());
		assertEquals("3x^0 -2x^2",outputs[1].getStringPolynom()); 
		outputs[0]=test.differentiate(pol1);
		outputs[1]=test.differentiate(pol2);
		assertEquals("6x^2 +4x^1",outputs[0].getStringPolynom());
		assertEquals("5x^4 +3x^2",outputs[1].getStringPolynom());
		outputs[0]=test.integrate(pol1);
		outputs[1]=test.integrate(pol2);
		assertEquals("0.5x^4 +0.7x^3",outputs[0].getStringPolynom());
		assertEquals("0.2x^6 +0.2x^4 +3x^1",outputs[1].getStringPolynom());
	}

}