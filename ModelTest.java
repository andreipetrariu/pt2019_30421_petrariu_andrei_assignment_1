package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import ui.UI;
class ModelTest {

	@Test
	void test() {
		UI testUI=new UI();
		int[][] values={{5,4,3,1,0},{2,3,1,2,10}};
		int[][] output=testUI.convert("2x^5+3x^4+x^3+2x+10");
		assertEquals(output[0][0],values[0][0]);
		assertEquals(output[0][1],values[0][1]);
		assertEquals(output[0][2],values[0][2]);
		assertEquals(output[0][3],values[0][3]);
		assertEquals(output[0][4],values[0][4]);
		assertEquals(output[1][0],values[1][0]);
		assertEquals(output[1][1],values[1][1]);
		assertEquals(output[1][2],values[1][2]);
		assertEquals(output[1][3],values[1][3]);
		assertEquals(output[1][4],values[1][4]);
	}

}
